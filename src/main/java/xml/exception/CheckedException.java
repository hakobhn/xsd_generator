package xml.exception;


public class CheckedException extends Exception {
    String message;

    public CheckedException(String message) {
        super(message);
        this.message = message;
    }
}
