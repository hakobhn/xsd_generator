package xml;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@SpringBootApplication
@PropertySource("classpath:application.properties")
public class XmlApplication {

	@Autowired
	private Environment environment;

	@Bean
	public String xmlFileStore() {
		return environment.getProperty("xml.file.store");
	}


	public static void main(String[] args) {
		SpringApplication.run(XmlApplication.class, args);
	}
}
