package xml.controller;

/**
 * Created by hakob on 3/5/18.
 */

import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.impl.xb.xsdschema.SchemaDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import xml.exception.CheckedException;
import xml.model.FileModel;
import xml.service.FileService;
import xml.util.SchemaGenerator;
import xml.util.StringUtil;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Controller
public class FileController {

    private static final Logger logger = LoggerFactory.getLogger(FileController.class);

    @Autowired
    private String xmlFileStore;

    @Autowired
    FileService fileService;

    @PostMapping("/home/upload")
    public ModelAndView xmlFileUpload(@RequestParam("file") MultipartFile file) {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("home/home");

        if (file.isEmpty()) {
            modelAndView.addObject("error",true);
            modelAndView.addObject("errorMessage", "Please select a file to upload");
        }

        try {

            if ( !( file.getContentType().equals("text/xml") || file.getContentType().equals("application/xml") ) ) {
                throw new CheckedException("Not xml file uploaded...");
            }

            SchemaGenerator generator = new SchemaGenerator();
            SchemaDocument schema = generator.generateSchema(file.getInputStream());

            byte[] bytes = generator.getSchema(schema).getBytes();
            String fileName = StringUtil.getFileName(file.getOriginalFilename())+".xsd";
            Path path = Paths.get(xmlFileStore + File.separator + fileName);
            Files.write(path, bytes);

            modelAndView.addObject("message", "xsd generated successfully...");
            modelAndView.addObject("schema", generator.getSchema(schema));
            modelAndView.addObject("fileName", fileName);

        } catch (IOException e) {
            modelAndView.addObject("error",true);
            modelAndView.addObject("errorMessage", "File is unreachable...");
        } catch (XmlException e) {
            modelAndView.addObject("error",true);
            modelAndView.addObject("errorMessage", "Please select a valid xml file to upload...");
        } catch (CheckedException e) {
            modelAndView.addObject("error",true);
            modelAndView.addObject("errorMessage", e.getMessage());
        }

        return modelAndView;
    }

    @Secured("USER")
    @GetMapping("/home/download/")
    public ModelAndView getFileList(HttpServletResponse response) throws IOException {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("file/files");

        try {

            List<FileModel> fileList = fileService.getFiles(xmlFileStore);

            modelAndView.addObject("files", fileList);

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            modelAndView.addObject("error",true);
            modelAndView.addObject("errorMessage",e.getMessage());
        }

        return modelAndView;
    }

    @GetMapping("/home/download_own/{fileName}")
    public ResponseEntity<Object> downloadOwnFile(@PathVariable String fileName,
                                               HttpServletResponse response) throws IOException {

        Path path = Paths.get(xmlFileStore + File.separator + StringUtil.getFileName(fileName+".xsd"));

        File xsdFile = path.toFile();

        HttpHeaders headers = new HttpHeaders();

        ByteArrayResource resource;
        try {
            resource = new ByteArrayResource(Files.readAllBytes(path));
        } catch (Exception e) {
            logger.info(e.getMessage(), e);
            return new ResponseEntity<Object>("Not found...",headers,HttpStatus.NOT_FOUND);
        }

        headers.setContentType(MediaType.TEXT_XML);
        headers.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName.replace(" ", "_"));

        return ResponseEntity.ok()
                .headers(headers)
                .contentLength(xsdFile.length())
                .contentType(MediaType.parseMediaType("text/xml"))
                .body(resource);
    }

    @Secured("USER")
    @GetMapping("/home/download/{fileName}")
    public ResponseEntity<Object> downloadFile(@PathVariable String fileName,
                                                    HttpServletResponse response) throws IOException {

        Path path = Paths.get(xmlFileStore + File.separator + StringUtil.getFileName(fileName+".xsd"));

        File xsdFile = path.toFile();

        HttpHeaders headers = new HttpHeaders();

        ByteArrayResource resource;
        try {
            resource = new ByteArrayResource(Files.readAllBytes(path));
        } catch (Exception e) {
            logger.info(e.getMessage(), e);
            return new ResponseEntity<Object>("Not found...",headers,HttpStatus.NOT_FOUND);
        }

        headers.setContentType(MediaType.TEXT_XML);
        headers.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName.replace(" ", "_"));

        return ResponseEntity.ok()
                .headers(headers)
                .contentLength(xsdFile.length())
                .contentType(MediaType.parseMediaType("text/xml"))
                .body(resource);
    }

}