package xml.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xml.model.FileModel;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by hakob on 3/6/18.
 */
public class FileUtil {

    private static final Logger logger = LoggerFactory.getLogger(FileUtil.class);

    public static List<FileModel> getFileNames(String dirPath) {

        File folder = new File(dirPath);
        File[] listOfFiles = folder.listFiles();

        List<FileModel> result = new ArrayList<>();

        BasicFileAttributes attr;

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {

                FileModel model = new FileModel();
                model.setName(listOfFiles[i].getName());

                try {
                    Path path = Paths.get(listOfFiles[i].getAbsolutePath());
                    attr = Files.readAttributes(path, BasicFileAttributes.class);
                    model.setCreateDate(new Date(attr.creationTime().toMillis()));
                } catch (IOException e) {
                    logger.error(e.getMessage(), e);
                    model.setCreateDate(null);
                }

                result.add(model);
            }
        }

        return result;
    }

}
