package xml.util;

/**
 * Created by hakob on 3/5/18.
 */
public class StringUtil {

    public static String getFileName(String fileName) {
        try {
            return fileName.substring(0, fileName.lastIndexOf("."));
        } catch (Exception e) {
            return "";
        }
    }

    public static String getExtension(String fileName) {
        try {
            return fileName.substring(fileName.lastIndexOf(".") + 1);
        } catch (Exception e) {
            return "";
        }
    }

//    public static String getPasswordHash(String text) throws Exception {
//
//        MessageDigest digest = MessageDigest.getInstance("SHA-256");
//        byte[] hash = digest.digest(text.getBytes(StandardCharsets.UTF_8));
//        return Base64.getEncoder().encodeToString(hash);
//    }

    public static String getPasswordHash(String text) throws Exception {

        return bytesToHex(encrypt(text));
    }

    public static byte[] encrypt(String x) throws Exception {
        java.security.MessageDigest d = null;
        d = java.security.MessageDigest.getInstance("SHA-1");
        d.reset();
        d.update(x.getBytes());
        return d.digest();
    }

    private static String bytesToHex(byte[] hash) {
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if(hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }

    public static void main(String[] args) {
        try {
            System.out.println( getPasswordHash("admin123456") );
            System.out.println( encrypt("123456") );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
