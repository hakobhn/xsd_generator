package xml.util;


import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.impl.inst2xsd.Inst2Xsd;
import org.apache.xmlbeans.impl.inst2xsd.Inst2XsdOptions;
import org.apache.xmlbeans.impl.xb.xsdschema.SchemaDocument;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

public class SchemaGenerator {


    public String getSchema(SchemaDocument schemaDocument) throws IOException {
        StringWriter writer = new StringWriter();
        schemaDocument.save(writer, new XmlOptions().setSavePrettyPrint());
        writer.close();

        return writer.toString();
    }

    public SchemaDocument generateSchema(File... files) throws XmlException, IOException {
        XmlObject[] xmlInstances = new XmlObject[files.length];

        for (int i = 0; i < files.length; i++) {
            xmlInstances[i] = XmlObject.Factory.parse(files[i]);
        }

        return inst2xsd(xmlInstances);
    }

    public SchemaDocument generateSchema(InputStream... inputStreams) throws XmlException, IOException {

        XmlObject[] xmlInstances = new XmlObject[inputStreams.length];

        for (int i = 0; i < inputStreams.length; i++) {
            xmlInstances[i] = XmlObject.Factory.parse(inputStreams[i]);
        }

        return inst2xsd(xmlInstances);
    }

    public SchemaDocument generateSchema(String... strs) throws XmlException, IOException {

        XmlObject[] xmlInstances = new XmlObject[strs.length];

        for (int i = 0; i < strs.length; i++) {
            // System.out.println("i= " + i + "  srts[i]= " + strs[i]);
            xmlInstances[i] = XmlObject.Factory.parse(strs[i]);
        }

        return inst2xsd(xmlInstances);
    }

    private SchemaDocument inst2xsd(XmlObject[] xmlInstances) throws IOException {

        Inst2XsdOptions inst2XsdOptions = new Inst2XsdOptions();
        inst2XsdOptions.setDesign(Inst2XsdOptions.DESIGN_RUSSIAN_DOLL);

        SchemaDocument[] schemaDocuments = Inst2Xsd.inst2xsd(xmlInstances, inst2XsdOptions);

        if (schemaDocuments != null && schemaDocuments.length > 0) {
            return schemaDocuments[0];
        }

        return null;
    }

    public static void main(String[] args) {
        try {
            SchemaGenerator generator = new SchemaGenerator();

            File xml1 = new File("./test/resources/test-data.xml");
            File xml2 = new File("./test/resources/test-data2.xml");
            File xml3 = new File("./test/resources/test-data3.xml");

            SchemaDocument schemaDocument = generator.generateSchema(xml1);
            String schema = generator.getSchema(schemaDocument);
            System.out.println(schema);
            System.out.println("*****************************************");

            schemaDocument = generator.generateSchema(xml2);
            schema = generator.getSchema(schemaDocument);
            System.out.println(schema);
            System.out.println("*****************************************");

//            schemaDocument = generator.generateSchema(xml3);
//            schema = generator.getSchema(schemaDocument);
//            System.out.println(schema);

            String xml4 = "<xml>\n" +
                    "    <news class=\"blue\">\n" +
                    "\n" +
                    "        <div>\n" +
                    "            <ul>\n" +
                    "                <li></li>\n" +
                    "                <li></li>\n" +
                    "                <li></li>\n" +
                    "                <li></li>\n" +
                    "            </ul>\n" +
                    "        </div>\n" +
                    "        <span>\n" +
                    "            bla\n" +
                    "            <table></table>\n" +
                    "        </span>\n" +
                    "\n" +
                    "    </news>\n" +
                    "\n" +
                    "</xml>";

            schemaDocument = generator.generateSchema(xml4);
            schema = generator.getSchema(schemaDocument);
            System.out.println(schema);
            System.out.println("*****************************************");

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}