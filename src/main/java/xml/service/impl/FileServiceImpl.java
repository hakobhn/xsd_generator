package xml.service.impl;

import org.springframework.stereotype.Service;
import xml.model.FileModel;
import xml.service.FileService;
import xml.util.FileUtil;

import java.util.List;

/**
 * Created by hakob on 3/5/18.
 */
@Service
public class FileServiceImpl implements FileService {


    @Override
    public List<FileModel> getFiles(String dirPath) {

        return FileUtil.getFileNames(dirPath);
    }


}
