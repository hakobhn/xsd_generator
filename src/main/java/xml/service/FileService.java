package xml.service;

import xml.model.FileModel;

import java.util.List;

/**
 * Created by hakob on 3/5/18.
 */
public interface FileService {

    List<FileModel> getFiles(String dirPath);

}
