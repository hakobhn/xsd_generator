$(document).ready(function() {

    changePageAndSize();

    $('#search_action').on('click', function(){
        $('#search_form').submit();
    });

    $('.dropdown_filter').on('change', function(){
        $('#search_form').submit();
    });

    if ($('.error_msg').text() != '') {
        $('html,body').animate({scrollTop: $('.error_msg').offset().top}, 'slow');
    } else if($('.chart_container').offset()) {
        $('html,body').animate({scrollTop: $('.chart_container').offset().top}, 'slow');
    } else if ($('.data_table').offset()) {
        $('html,body').animate({scrollTop: $('.data_table').offset().top}, 'slow');
    }

});


function resetSorteds() {
    $('.sorting_asc').each(function(){
        $(this).attr("class", "sorting");
    });
    $('.sorting_desc').each(function(){
        $(this).attr("class", "sorting");
    });
}


function changePageAndSize() {
    $('.pageSizeSelect').change(function(evt) {
        var href = "?pageSize=" + this.value + "&page=1";
        var orderCol = $("input[name='orderCol']").val();
        if(orderCol) {
            href += "&orderCol="+orderCol;
        }
        var order = $("input[name='order']").val();
        if(order) {
            href += "&order="+order;
        }
        var search = $("input[name='search']").val();
        if(search) {
            href += "&search="+search;
        }
        var clientType = $("input[name='clientType']").val();
        if(clientType) {
            href += "&clientType="+clientType;
        }
        var userStatus = $("input[name='userStatus']").val();
        if(userStatus) {
            href += "&userStatus="+userStatus;
        }
        var claimable = $("input[name='claimable']").is(':checked');
        if(claimable) {
            href += "&claimable="+claimable;
        }

        window.location.replace(href);
    });
}